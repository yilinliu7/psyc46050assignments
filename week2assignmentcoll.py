import pandas as pd
df=pd.read_csv('Combined.csv')
mean_value = (df['Better']+df['Matter']).apply(lambda x:x/2)
from scipy.stats import binom_test
result = binom_test(mean_value.sum(), n=len(mean_value), p=0.5)
result
install_requires=[
    'pandas'
    'scipy.stats'
]


